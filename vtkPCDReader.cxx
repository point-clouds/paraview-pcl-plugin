//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
#include "vtkPCDReader.h"

#include "vtkPolyData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkSmartPointer.h"
#include "vtkNew.h"

#include "vtkPCLConversions.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPCDReader);

//----------------------------------------------------------------------------
vtkPCDReader::vtkPCDReader()
{
  this->FileName = 0;
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkPCDReader::~vtkPCDReader()
{
  this->SetFileName(NULL);
}

//----------------------------------------------------------------------------
int vtkPCDReader::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkDataSet *output = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if (!this->GetFileName())
    {
    vtkErrorMacro("Filename is not set");
    return 0;
    }

  vtkSmartPointer<vtkPolyData> polyData = vtkPCLConversions::PolyDataFromPCDFile(this->GetFileName());

  if (!polyData)
    {
    vtkErrorMacro("Failed to read pcd file: " << this->GetFileName());
    }

  output->ShallowCopy(polyData);
  return 1;
}

//----------------------------------------------------------------------------
void vtkPCDReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
#include "vtkPCLEuclideanClusterExtraction.h"
#include "vtkPCLConversions.h"

#include "vtkPolyData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkPointData.h"

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

//----------------------------------------------------------------------------
namespace {

void ApplyEuclideanClusterExtraction(
                      pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud,
                      double clusterTolerance, double minClusterSize, double maxClusterSize,
                      std::vector<pcl::PointIndices>& clusterIndices)
{
  if (!cloud || !cloud->points.size())
    {
    return;
    }

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud(cloud);
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> clusterFilter;
  clusterFilter.setClusterTolerance(clusterTolerance);
  clusterFilter.setMinClusterSize(minClusterSize);
  clusterFilter.setMaxClusterSize(maxClusterSize);
  clusterFilter.setSearchMethod(tree);
  clusterFilter.setInputCloud(cloud);
  clusterFilter.extract(clusterIndices);
}

}

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPCLEuclideanClusterExtraction);

//----------------------------------------------------------------------------
vtkPCLEuclideanClusterExtraction::vtkPCLEuclideanClusterExtraction()
{
  this->ClusterTolerance = 0.05;
  this->MinClusterSize = 100;
  this->MaxClusterSize = 100000;
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkPCLEuclideanClusterExtraction::~vtkPCLEuclideanClusterExtraction()
{
}

//----------------------------------------------------------------------------
int vtkPCLEuclideanClusterExtraction::RequestData(
  vtkInformation* vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // get input and output data objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkPolyData *input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // perform euclidean cluster extraction
  std::vector<pcl::PointIndices> clusterIndices;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = vtkPCLConversions::PointCloudFromPolyData(input);
  ApplyEuclideanClusterExtraction(cloud, this->ClusterTolerance, this->MinClusterSize, this->MaxClusterSize, clusterIndices);

  // pass thru input add labels
  vtkSmartPointer<vtkIntArray> labels = vtkPCLConversions::NewLabelsArray(clusterIndices, input->GetNumberOfPoints());
  labels->SetName("cluster_labels");
  output->ShallowCopy(input);
  output->GetPointData()->AddArray(labels);

  return 1;
}

//----------------------------------------------------------------------------
void vtkPCLEuclideanClusterExtraction::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

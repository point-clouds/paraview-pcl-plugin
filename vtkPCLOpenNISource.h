//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLOpenNISource -
// .SECTION Description
//

#ifndef __vtkPCLOpenNISource_h
#define __vtkPCLOpenNISource_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>


class VTKPCLFILTERS_EXPORT vtkPCLOpenNISource : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLOpenNISource, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLOpenNISource *New();

  bool HasNewData();

  void Poll();

  void StartGrabber();
  void StopGrabber();

protected:

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkPCLOpenNISource();
  virtual ~vtkPCLOpenNISource();

private:
  vtkPCLOpenNISource(const vtkPCLOpenNISource&);  // Not implemented.
  void operator=(const vtkPCLOpenNISource&);  // Not implemented.

  class vtkInternal;
  vtkInternal * Internal;
};

#endif

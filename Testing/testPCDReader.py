#=========================================================================
#
# Copyright 2012,2013,2014 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=========================================================================
# This script reads a PCD files and runs the
# PCL to vtkPolyData conversion benchmark

import sys
if len(sys.argv) < 2:
    print 'Usage: %s <filename.pcd>' % sys.argv[0]
    sys.exit(1)

from vtkPCLFiltersPython import *

pcdFile = sys.argv[1]

r = vtkPCDReader()
r.SetFileName(pcdFile)
r.Update()

p = r.GetOutput()
vtkPCLConversions.PerformPointCloudConversionBenchmark(p)

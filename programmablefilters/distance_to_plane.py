#=========================================================================
#
# Copyright 2012,2013,2014 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=========================================================================

import numpy
import vtkPCLFiltersPython as pcl

# perform plane segmentation
f = pcl.vtkPCLSACSegmentationPlane()
f.SetInput(inputs[0].VTKObject)
f.SetDistanceThreshold(0.01)
f.Update()
origin = f.GetPlaneOrigin()
normal = f.GetPlaneNormal()

# for each point, compute signed distance to plane
dist = numpy.dot(inputs[0].Points - origin, normal)

# flip the sign if needed (dot normal with Y axis)
if numpy.dot(normal, [0,1,0]) > 0:
    dist *= -1.0

# pass thru the input data and append the dist_to_plane array
output.ShallowCopy(f.GetOutput())
output.PointData.append(dist, 'dist_to_plane')

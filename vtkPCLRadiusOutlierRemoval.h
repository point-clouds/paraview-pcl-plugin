//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLRadiusOutlierRemoval -
// .SECTION Description
//

#ifndef __vtkPCLRadiusOutlierRemoval_h
#define __vtkPCLRadiusOutlierRemoval_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>


class  VTKPCLFILTERS_EXPORT vtkPCLRadiusOutlierRemoval : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLRadiusOutlierRemoval, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLRadiusOutlierRemoval *New();

  vtkSetMacro(SearchRadius, double);
  vtkGetMacro(SearchRadius, double);

  vtkSetMacro(NeighborsInSearchRadius, int);
  vtkGetMacro(NeighborsInSearchRadius, int);

protected:

  double SearchRadius;
  int NeighborsInSearchRadius;

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkPCLRadiusOutlierRemoval();
  virtual ~vtkPCLRadiusOutlierRemoval();

private:
  vtkPCLRadiusOutlierRemoval(const vtkPCLRadiusOutlierRemoval&);  // Not implemented.
  void operator=(const vtkPCLRadiusOutlierRemoval&);  // Not implemented.
};

#endif



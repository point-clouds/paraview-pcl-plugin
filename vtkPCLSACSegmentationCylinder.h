//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLSACSegmentationCylinder -
// .SECTION Description
//

#ifndef __vtkPCLSACSegmentationCylinder_h
#define __vtkPCLSACSegmentationCylinder_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>

class VTKPCLFILTERS_EXPORT vtkPCLSACSegmentationCylinder : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLSACSegmentationCylinder, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLSACSegmentationCylinder *New();

  vtkSetMacro(DistanceThreshold, double);
  vtkGetMacro(DistanceThreshold, double);

  vtkSetMacro(MaxIterations, int);
  vtkGetMacro(MaxIterations, int);

  vtkSetMacro(RadiusLimit, double);
  vtkGetMacro(RadiusLimit, double);

  vtkSetMacro(SearchRadius, double);
  vtkGetMacro(SearchRadius, double);

  vtkSetMacro(NormalDistanceWeight, double);
  vtkGetMacro(NormalDistanceWeight, double);

protected:

  double NormalDistanceWeight;
  double DistanceThreshold;
  double RadiusLimit;
  double SearchRadius;
  
  int MaxIterations;

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);
  vtkPCLSACSegmentationCylinder();
  virtual ~vtkPCLSACSegmentationCylinder();

private:
  vtkPCLSACSegmentationCylinder(const vtkPCLSACSegmentationCylinder&);  // Not implemented.
  void operator=(const vtkPCLSACSegmentationCylinder&);  // Not implemented.
};

#endif



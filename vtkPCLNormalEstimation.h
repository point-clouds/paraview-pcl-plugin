//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLNormalEstimation -
// .SECTION Description
//

#ifndef __vtkPCLNormalEstimation_h
#define __vtkPCLNormalEstimation_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>


class VTKPCLFILTERS_EXPORT vtkPCLNormalEstimation : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLNormalEstimation, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLNormalEstimation *New();

  vtkSetMacro(SearchRadius, double);
  vtkGetMacro(SearchRadius, double);


protected:

  double SearchRadius;

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkPCLNormalEstimation();
  virtual ~vtkPCLNormalEstimation();


  virtual int FillInputPortInformation(int port, vtkInformation* info);

private:
  vtkPCLNormalEstimation(const vtkPCLNormalEstimation&);  // Not implemented.
  void operator=(const vtkPCLNormalEstimation&);  // Not implemented.
};

#endif

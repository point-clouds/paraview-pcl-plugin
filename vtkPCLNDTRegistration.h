//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLNDTRegistration -
// .SECTION Description
//

#ifndef __vtkPCLNDTRegistration_h
#define __vtkPCLNDTRegistration_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>


class VTKPCLFILTERS_EXPORT vtkPCLNDTRegistration : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLNDTRegistration, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLNDTRegistration *New();

  vtkSetMacro(StepSize, double);
  vtkGetMacro(StepSize, double);

  vtkSetMacro(Resolution, double);
  vtkGetMacro(Resolution, double);

  vtkSetMacro(MaxIteration, int);
  vtkGetMacro(MaxIteration, int);

  vtkSetVector3Macro(InitTranslation, double);
  vtkGetVector3Macro(InitTranslation, double);

protected:

  double StepSize;
  double Resolution;
  int MaxIteration;
  double InitTranslation[3];

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkPCLNDTRegistration();
  virtual ~vtkPCLNDTRegistration();

private:
  vtkPCLNDTRegistration(const vtkPCLNDTRegistration&);  // Not implemented.
  void operator=(const vtkPCLNDTRegistration&);  // Not implemented.
};

#endif



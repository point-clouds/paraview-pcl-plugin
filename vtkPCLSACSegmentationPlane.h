//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
// .NAME vtkPCLSACSegmentationPlane -
// .SECTION Description
//

#ifndef __vtkPCLSACSegmentationPlane_h
#define __vtkPCLSACSegmentationPlane_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>

class VTKPCLFILTERS_EXPORT vtkPCLSACSegmentationPlane : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLSACSegmentationPlane, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLSACSegmentationPlane *New();

  vtkSetMacro(DistanceThreshold, double);
  vtkGetMacro(DistanceThreshold, double);

  vtkSetMacro(MaxIterations, int);
  vtkGetMacro(MaxIterations, int);

  vtkGetVector4Macro(PlaneCoefficients, double);
  vtkGetVector3Macro(PlaneOrigin, double);
  vtkGetVector3Macro(PlaneNormal, double);

  vtkSetMacro(PerpendicularConstraintEnabled, bool);
  vtkGetMacro(PerpendicularConstraintEnabled, bool);

  vtkSetMacro(AngleEpsilon, double);
  vtkGetMacro(AngleEpsilon, double);

  vtkGetVector3Macro(PerpendicularAxis, double);
  vtkSetVector3Macro(PerpendicularAxis, double);

protected:

  double DistanceThreshold;
  int MaxIterations;

  bool PerpendicularConstraintEnabled;
  double PerpendicularAxis[3];
  double AngleEpsilon;

  double PlaneCoefficients[4];
  double PlaneOrigin[3];
  double PlaneNormal[3];

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);


  vtkPCLSACSegmentationPlane();
  virtual ~vtkPCLSACSegmentationPlane();

private:
  vtkPCLSACSegmentationPlane(const vtkPCLSACSegmentationPlane&);  // Not implemented.
  void operator=(const vtkPCLSACSegmentationPlane&);  // Not implemented.
};

#endif



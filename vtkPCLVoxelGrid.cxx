//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
#include "vtkPCLVoxelGrid.h"
#include "vtkPCLConversions.h"

#include "vtkPolyData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkSmartPointer.h"
#include "vtkNew.h"
#include "vtkAlgorithmOutput.h"

#include <pcl/filters/voxel_grid.h>

//----------------------------------------------------------------------------
namespace {

pcl::PointCloud<pcl::PointXYZ>::Ptr ApplyVoxelGrid(
                      pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud,
                      double leafSize[3])
{
  pcl::VoxelGrid<pcl::PointXYZ> voxelGrid;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFiltered(new pcl::PointCloud<pcl::PointXYZ>);
  voxelGrid.setInputCloud(cloud);
  voxelGrid.setLeafSize(leafSize[0], leafSize[1], leafSize[2]);
  voxelGrid.filter(*cloudFiltered);
  return cloudFiltered;
}

}

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPCLVoxelGrid);

//----------------------------------------------------------------------------
vtkPCLVoxelGrid::vtkPCLVoxelGrid()
{
  this->LeafSize[0] = 0.01;
  this->LeafSize[1] = 0.01;
  this->LeafSize[2] = 0.01;
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkPCLVoxelGrid::~vtkPCLVoxelGrid()
{
}

//----------------------------------------------------------------------------
int vtkPCLVoxelGrid::RequestData(
  vtkInformation* vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkPolyData *input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkPolyData *output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = vtkPCLConversions::PointCloudFromPolyData(input);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFiltered = ApplyVoxelGrid(cloud, this->LeafSize);

  output->ShallowCopy(vtkPCLConversions::PolyDataFromPointCloud(cloudFiltered));
  return 1;
}

//----------------------------------------------------------------------------
void vtkPCLVoxelGrid::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkAnnotateOBBs -
// .SECTION Description
//

#ifndef __vtkAnnotateOBBs_h
#define __vtkAnnotateOBBs_h
#include <vtkPCLFiltersModule.h>
#include <vtkPolyDataAlgorithm.h>


class VTKPCLFILTERS_EXPORT vtkAnnotateOBBs : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkAnnotateOBBs, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkAnnotateOBBs *New();

  vtkGetMacro(AnnotateLabelZero, bool);
  vtkSetMacro(AnnotateLabelZero, bool);

protected:

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkAnnotateOBBs();
  virtual ~vtkAnnotateOBBs();

  bool AnnotateLabelZero;

private:
  vtkAnnotateOBBs(const vtkAnnotateOBBs&);  // Not implemented.
  void operator=(const vtkAnnotateOBBs&);  // Not implemented.
};

#endif



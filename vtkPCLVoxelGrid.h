//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
// .NAME vtkPCLVoxelGrid -
// .SECTION Description
//

#ifndef __vtkPCLVoxelGrid_h
#define __vtkPCLVoxelGrid_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>

class VTKPCLFILTERS_EXPORT vtkPCLVoxelGrid : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLVoxelGrid, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLVoxelGrid *New();

  vtkSetVector3Macro(LeafSize, double);
  vtkGetVector3Macro(LeafSize, double);

protected:

  double LeafSize[3];

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);


  vtkPCLVoxelGrid();
  virtual ~vtkPCLVoxelGrid();

private:
  vtkPCLVoxelGrid(const vtkPCLVoxelGrid&);  // Not implemented.
  void operator=(const vtkPCLVoxelGrid&);  // Not implemented.
};

#endif



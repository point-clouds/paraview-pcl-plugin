//=========================================================================
//
// Copyright 2012,2013,2014 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================
//
// .NAME vtkPCLEuclideanClusterExtraction -
// .SECTION Description
//

#ifndef __vtkPCLEuclideanClusterExtraction_h
#define __vtkPCLEuclideanClusterExtraction_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkPCLFiltersModule.h>


class VTKPCLFILTERS_EXPORT vtkPCLEuclideanClusterExtraction : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPCLEuclideanClusterExtraction, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPCLEuclideanClusterExtraction *New();

  vtkSetMacro(ClusterTolerance, double);
  vtkGetMacro(ClusterTolerance, double);

  vtkSetMacro(MinClusterSize, int);
  vtkGetMacro(MinClusterSize, int);

  vtkSetMacro(MaxClusterSize, int);
  vtkGetMacro(MaxClusterSize, int);

protected:

  double ClusterTolerance;
  int MinClusterSize;
  int MaxClusterSize;

  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  vtkPCLEuclideanClusterExtraction();
  virtual ~vtkPCLEuclideanClusterExtraction();

private:
  vtkPCLEuclideanClusterExtraction(const vtkPCLEuclideanClusterExtraction&);  // Not implemented.
  void operator=(const vtkPCLEuclideanClusterExtraction&);  // Not implemented.
};

#endif


